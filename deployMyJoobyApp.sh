#!/bin/bash

#assign variables
AWS_DEFAULT_REGION=us-east-1
az1=us-east-1a
az2=us-east-1b
cidr1=10.0.0.0/16
cidr2=10.0.10.0/24
cidr3=10.0.20.0/24
sgName=my-sg
sgElbName=my-sg-elb
port=8080
elbName=my-elb
tgName=my-targets
repoName=my-repo
servRoleName=AWSServiceRoleForECS
ec2RoleName=ecsRole
ec2PolName=ecsRolePolicy
clName=my-cluster
imageAmi=ami-0b9a214f40c38d5eb
insType=t2.micro
taskName=jooby
serviceName=my-service
clSize=1
domName=resources

#get console parameters

while [ "$1" != "" ]; do
case $1 in
-c | --clusterSize )  shift
                      clSize=$1
                      ;;
-d | --domainName )   shift
                      domName=$1
                      ;;
esac
shift
done

echo "Creating resources:
	vpc		 - 1
	internet gateway - 1
	subnets		 - 2
	security groups	 - 2
	load balancer	 - 1
	target group	 - 1
	repository	 - 1
	instance	 - ${clSize}
	cluster		 - 1"

#create a role
aws iam create-role --role-name $ec2RoleName --assume-role-policy-document file://ecsPolicy.json
aws iam put-role-policy --role-name $ec2RoleName --policy-name $ec2PolName  --policy-document file://rolePolicy.json
aws iam create-instance-profile --instance-profile-name $ec2RoleName
aws iam add-role-to-instance-profile --instance-profile-name $ec2RoleName  --role-name $ec2RoleName
arnEC2Role=$(aws iam get-role --role-name $ec2RoleName --query 'Role.Arn' --output text)
echo '#!/bin/bash' > $domName.txt
echo "ec2RoleName=${ec2RoleName}" >> $domName.txt
echo "ec2PolName=${ec2PolName}" >> $domName.txt
echo "arnEC2Role=${arnEC2Role}" >> $domName.txt
 
#create a vpc
vpc=$(aws ec2 create-vpc --cidr-block $cidr1)
if [ "$?" -ne 0 ]; then echo "a vpc creation failed"; exit 1; fi
vpcId=$(echo $vpc | grep -o '"VpcId": "[^"]*"' | cut -d'"' -f4)
echo "vpcId=${vpcId}" >> $domName.txt
echo "--- The vpc ${vpcId} created ---"

#create an internet gateway and attach to the vpc
ig=$(aws ec2 create-internet-gateway)
if [ "$?" -ne 0 ]; then echo "an internet gateway creation failed"; exit 1; fi
igId=$(echo $ig | grep -o '"InternetGatewayId": "[^"]*"' | cut -d'"' -f4)
echo "igId=${igId}" >> $domName.txt
aws ec2 attach-internet-gateway --internet-gateway-id $igId --vpc-id $vpcId
echo "--- The internet gateway ${igId} created ---"

#create a subnet
sub=$(aws ec2 create-subnet --vpc-id $vpcId --cidr-block $cidr2 --availability-zone $az1)
if [ "$?" -ne 0 ]; then echo "a subnet creation failed"; exit 1; fi
subId=$(echo $sub | grep -o '"SubnetId": "[^"]*"' | cut -d'"' -f4)
echo "subId=${subId}" >> $domName.txt
aws ec2 modify-subnet-attribute --subnet-id $subId --map-public-ip-on-launch
echo "--- The subnet ${subId} created ---"

sub2=$(aws ec2 create-subnet --vpc-id $vpcId --cidr-block $cidr3 --availability-zone $az2)
if [ "$?" -ne 0 ]; then echo "a subnet creation failed"; exit 1; fi
subId2=$(echo $sub2 | grep -o '"SubnetId": "[^"]*"' | cut -d'"' -f4)
echo "subId2=${subId2}" >> $domName.txt
aws ec2 modify-subnet-attribute --subnet-id $subId2 --map-public-ip-on-launch
echo "--- The subnet ${subId2} created ---"

#create security groups
sg=$(aws ec2 create-security-group --group-name $sgName --vpc-id $vpcId --description $sgName)
if [ "$?" -ne 0 ]; then echo "a security group creation failed"; exit 1; fi
sgId=$(echo $sg | cut -d'"' -f4)
echo "sgId=${sgId}" >> $domName.txt
echo "--- The security group ${sgId} created ---"

sgElb=$(aws ec2 create-security-group --group-name $sgElbName --vpc-id $vpcId --description $sgElbName)
if [ "$?" -ne 0 ]; then echo "a security group creation failed"; exit 1; fi
sgIdElb=$(echo $sgElb | cut -d'"' -f4)
echo "sgIdElb=${sgIdElb}" >> $domName.txt
echo "--- The security group ${sgIdElb} created ---"

#add a rule for security group
aws ec2 authorize-security-group-ingress --group-id $sgIdElb --protocol tcp --port $port --cidr 0.0.0.0/0

#create a load balancer
elb=$(aws elbv2 create-load-balancer --name $elbName --subnets $subId $subId2 --security-groups $sgIdElb)
if [ "$?" -ne 0 ]; then echo "a load balancer creation failed"; exit 1; fi
elbArn=$(echo $elb | grep -o '"LoadBalancerArn": "[^"]*"' | cut -d'"' -f4)
echo "elbArn=${elbArn}" >> $domName.txt
echo "--- The load balancer ${elbArn} created ---"

#create a target group
tg=$(aws elbv2 create-target-group --name $tgName --protocol HTTP --port $port --vpc-id $vpcId)
if [ "$?" -ne 0 ]; then echo "a target group creation failed"; exit 1; fi
tgArn=$(echo $tg | grep -o '"TargetGroupArn": "[^"]*"' | cut -d'"' -f4)
echo "tgArn=${tgArn}" >> $domName.txt
echo "--- The target group ${tgArn} created ---"

#create a listener
lis=$(aws elbv2 create-listener --load-balancer-arn $elbArn --protocol HTTP --port $port --default-actions Type=forward,TargetGroupArn=$tgArn)
if [ "$?" -ne 0 ]; then echo "a listener creation failed"; exit 1; fi
lisArn=$(aws elbv2 describe-listeners --load-balancer-arn $elbArn --query 'Listeners[].ListenerArn' --output text)
echo "lisArn=${lisArn}" >> $domName.txt

#allow traffic to hit the container instance
aws ec2 authorize-security-group-ingress --group-id $sgId --protocol tcp --port 1-65535 --source-group $sgIdElb

#associate a route table with subnets and add a route to the internet gateway
rt=$(aws ec2 describe-route-tables --filters "Name=vpc-id,Values=${vpcId}")
rtId=$(echo $rt | grep -o '"RouteTableId": "[^"]*",' | cut -d'"' -f4)
aws ec2 associate-route-table --route-table-id $rtId --subnet-id $subId
aws ec2 associate-route-table --route-table-id $rtId --subnet-id $subId2
aws ec2 create-route --route-table-id $rtId --destination-cidr-block 0.0.0.0/0 --gateway-id $igId

#create a repository
repo=$(aws ecr create-repository --repository-name $repoName)
if [ "$?" -ne 0 ]; then echo "a repo creation failed"; exit 1; fi
repoUri=$(aws ecr describe-repositories --repository-names $repoName --query 'repositories[0].repositoryUri' --output text)
echo "repoName=${repoName}" >> $domName.txt 
echo "repoUri=${repoUri}" >> $domName.txt
echo "--- The repository ${repoUri} created ---"

#create a docker image and push to the repository
mkdir temp && cd temp

mvn archetype:generate -B -DgroupId=com.mycompany -DartifactId=my-app -Dversion=1.0-SNAPSHOT -DarchetypeArtifactId=jooby-archetype -DarchetypeGroupId=org.jooby -DarchetypeVersion=1.5.1

cd my-app && mvn compile jib:build -Dimage=$repoUri && cd ../../
echo "--- A docker image created and pushed to the repository---"

#create a cluster

aws ecs create-cluster --cluster-name $clName
if [ "$?" -ne 0 ]; then echo "a cluster creation failed"; exit 1; fi
echo "clName=${clName}" >> $domName.txt
sed -i 's/\(^.*=\)\(.*\)\(\s>>.*$\)/\1'"${clName}"'\3/g' ecs-bootstrap.txt
echo "--- The cluster ${clName} created ---"

#run instances
aws ec2 run-instances \
--image-id $imageAmi \
--security-group-ids $sgId \
--instance-type $insType \
--subnet-id $subId \
--iam-instance-profile Name=$ec2RoleName \
--user-data file://ecs-bootstrap.txt \
--count $clSize
if [ "$?" -ne 0 ]; then echo "an instance creation failed"; exit 1; fi
echo "--- Instances created ---"

#create a task
sed -i 's/\(^.*\"family\": "\)\([^"]*\)\(\".*$\)/\1'"${taskName}"'\3/g' task-definition.json
sed -i 's/\(^.*\"name\": "\)\([^"]*\)\(\".*$\)/\1'"${taskName}"'\3/g' task-definition.json
sed -i 's~\(^.*\"image\": "\)\([^"]*\)\(\".*$\)~\1'"${repoUri}"'\3~g' task-definition.json
aws ecs register-task-definition --cli-input-json file://task-definition.json
echo "taskName=${taskName}" >> $domName.txt

#create a service
sed -i 's/\(^.*\"cluster\": "\)\([^"]*\)\(\".*$\)/\1'"${clName}"'\3/g' ecs-service.json
sed -i 's/\(^.*\"serviceName\": "\)\([^"]*\)\(\".*$\)/\1'"${serviceName}"'\3/g' ecs-service.json
sed -i 's/\(^.*\"taskDefinition\": "\)\([^"]*\)\(\".*$\)/\1'"${taskName}"'\3/g' ecs-service.json
sed -i 's~\(^.*\"targetGroupArn\": "\)\([^"]*\)\(\".*$\)~\1'"${tgArn}"'\3~g' ecs-service.json
sed -i 's/\(^.*\"containerName\": "\)\([^"]*\)\(\".*$\)/\1'"${taskName}"'\3/g' ecs-service.json
arnServRole=$(aws iam get-role --role-name $servRoleName --query 'Role.Arn' --output text)
sed -i 's~\(^.*\"role\": "\)\([^"]*\)\(\".*$\)~\1'"${arnServRole}"'\3~g' ecs-service.json
aws ecs create-service --cli-input-json file://ecs-service.json
echo "serviceName=${serviceName}" >> $domName.txt

#print an information
elbDNS=$(aws elbv2 describe-load-balancers --load-balancer-arns $elbArn --query 'LoadBalancers[].DNSName' --output text)
echo "-----------------------------------------------------"
echo "An application will be running on '${elbDNS}:${port}' in a few minutes"
