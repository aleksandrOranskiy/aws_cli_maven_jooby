#!/bin/bash

#delete cluster
domName=resources

while [ "$1" != "" ]; do
case $1 in
-d | --domainName )   shift
                      domName=$1
                      ;;
esac
shift
done

source $domName.txt

echo "Deleting resources"
echo "--------------------------"
aws ecs update-service --cluster $clName --service $serviceName --desired-count 0 &> delete.log

aws ecs delete-service --cluster $clName --service $serviceName &>> delete.log

contIds=$(aws ecs list-container-instances --cluster $clName --query 'containerInstanceArns[]' --output text)
array=(${contIds// / })

for i in "${!array[@]}"
do
x=$(echo "${array[i]}" | sed -e 's/^[^\/]*\/\(.*\)$/\1/g')
aws ecs deregister-container-instance --cluster $clName --container-instance $x --force &>> delete.log
done

echo "--- Containers deleted ---"

aws ecs delete-cluster --cluster $clName &>> delete.log
echo "--- A cluster deleted ---"

aws ecs deregister-task-definition --task-definition "${taskName}:1" &>> delete.log

if aws ecr delete-repository --force --repository-name $repoName &>> delete.log ; then
  echo "--- A repository deleted ---"
else
  echo "Deleting of a repository failed"
fi

declare -a nodeIds=$(aws ec2 describe-instances --query 'Reservations[].Instances[].InstanceId' --output text)

if for i in "${nodeIds[@]}"
do 
aws ec2 terminate-instances --instance-ids $i &>> delete.log
done ; then
  echo "--- Instances are terminating ---"
else
  echo "Terminating of instances failed"
fi

aws elbv2 delete-listener --listener-arn $lisArn &>> delete.log

if aws elbv2 delete-load-balancer --load-balancer-arn $elbArn &>> delete.log ; then
  echo "--- A load balancer deleted ---"
else
  echo "Deleting of a load balancer failed"
fi

if aws elbv2 delete-target-group --target-group-arn $tgArn &>> delete.log ; then
  echo "--- A target group deleted ---"
else
  echo "Deleting of a target group failed"
fi

echo "Waiting for a completion of terminating instances" && sleep 3m

aws ec2 detach-internet-gateway --internet-gateway-id $igId --vpc-id $vpcId
if aws ec2 delete-internet-gateway --internet-gateway-id $igId ; then
  echo "--- An internet gateway deleted ---"
else
  echo "Deleting of an internet gateway failed"
fi

if aws ec2 delete-security-group --group-id $sgId ; then
  echo "--- A security group deleted ---"
else
  echo "Deleting of a security group failed"
fi

if aws ec2 delete-security-group --group-id $sgIdElb ; then
  echo "--- A security group deleted ---"
else
  echo "Deleting of a security group failed"
fi

if aws ec2 delete-subnet --subnet-id $subId ; then
  echo "--- A subnet deleted ---"
else
  echo "Deleting of a subnet failed"
fi

if aws ec2 delete-subnet --subnet-id $subId2 ; then
  echo "--- A subnet deleted ---"
else
  echo "Deleting of a subnet failed"
fi

if aws ec2 delete-vpc --vpc-id $vpcId &>> delete.log ; then
  echo "--- A vpc deleted ---"
else
  echo "Deleting of a vpc failed"
fi

aws iam delete-role-policy --role-name $ec2RoleName --policy-name $ec2PolName &>> delete.log
aws iam remove-role-from-instance-profile --instance-profile-name $ec2RoleName --role-name $ec2RoleName &>> delete.log
aws iam delete-instance-profile --instance-profile-name $ec2RoleName &>> delete.log
if aws iam delete-role --role-name $ec2RoleName &>> delete.log ; then
  echo "--- A role deleted ---"
else
  echo "Deleting of a role failed"
fi

rm -rf temp/
echo "--- A folder temp deleted ---"
echo "---------------------------------"
